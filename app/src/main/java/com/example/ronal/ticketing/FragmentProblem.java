package com.example.ronal.ticketing;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * Created by Nesa Alenia on 23/04/2018.
 */

public class FragmentProblem extends Fragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        return super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.layout_fragmentproblem, container, false);
        initRecyclerView(view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().setTitle("Problem");
    }

    private void initRecyclerView(View view) {
        AccessPointSingleton.getInstance().isHistory = false;
        RecyclerView recyclerView = view.findViewById(R.id.recyclerview_problem);
        AccessPointSingleton.getInstance().fragmentActivity = this.getActivity();
        AccessPointSingleton.getInstance().recyclerView = recyclerView;
    }
}
