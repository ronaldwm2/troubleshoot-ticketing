package com.example.ronal.ticketing;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GuestActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,AdapterView.OnItemSelectedListener {
    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_IMAGE_GALLERY = 2;
    String problem,problem2;
    String detail;
    EditText etGlobal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AccessPointSingleton.getInstance().setCurrentContext(GuestActivity.this);
        setContentView(R.layout.activity_guest);
        setTitle("Halaman Bantuan");


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        TextView nama = headerView.findViewById(R.id.namaOrangGuest);
        TextView jabatan = headerView.findViewById(R.id.jabatanGuest);
        navigationView.setNavigationItemSelectedListener(this);

        final TextView ipAndIdTV = findViewById(R.id.currentIpTextView);
        SharedPreferences preferences
                = getSharedPreferences("LOGIN", MODE_PRIVATE);

        if (preferences.getString("LEVEL", "").equals("admin")) {
            ipAndIdTV.setText("Admin");
            nama.setText("Admin");
            jabatan.setText("Admin");
        }else if (!preferences.getString("LEVEL","").equals("")){
            ipAndIdTV.setText(preferences.getString("ID","").toString());
            nama.setText(preferences.getString("ID","").toString());
            jabatan.setText(preferences.getString("LEVEL","").toString());
        }
        else {
            ipAndIdTV.setText(AccessPointSingleton.getLocalIpAddress());
            nama.setText(AccessPointSingleton.getLocalIpAddress());
            jabatan.setText("");
        }

        Spinner categorySpinner = findViewById(R.id.spinnerCategory);
        categorySpinner.setOnItemSelectedListener(this);

        Button btnGallery = findViewById(R.id.galleryButton);

        btnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, REQUEST_IMAGE_GALLERY);
            }
        });

        Button btnPhoto = findViewById(R.id.takePhotoBtn);
        btnPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            }
        });

        Button btn = findViewById(R.id.submitProblemBtn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(GuestActivity.this);
                alertDialog.setCancelable(true);
                alertDialog.setTitle("Confirmation");
                alertDialog.setMessage("Apakah problem sudah benar?");
                alertDialog.setPositiveButton("Benar",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ImageView iv = findViewById(R.id.imageView);
                                if(problem.equals("Lainya") || problem.equals("Email Tidak Berfungsi") || (problem.equals("Perlengkapan Rusak") &&problem2.equals("Lainya"))){
                                    detail = etGlobal.getText().toString();
                                }
                                new Token(problem,ipAndIdTV.getText().toString(),"",detail).execute();
                            }
                        });
                alertDialog.setNegativeButton("Salah", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });

                AlertDialog dialog = alertDialog.create();
                dialog.show();
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        final LinearLayout information = findViewById(R.id.Information);
        final LinearLayout headerInformation = findViewById(R.id.InformationHeader);

        Spinner spinner = (Spinner) adapterView;
        if (spinner.getId() == R.id.spinnerCategory) {
            problem = adapterView.getItemAtPosition(i).toString();
            information.removeAllViews();
            headerInformation.removeAllViews();
            if (problem.equals("Perlengkapan Rusak")) {
                TextView txt = new TextView(this);
                txt.setText("Alat");
                txt.setTextSize(16);
                txt.setTextColor(Color.BLACK);
                LinearLayout.LayoutParams txtParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                txtParams.weight = 3.0f;
                txt.setLayoutParams(txtParams);
                headerInformation.addView(txt);

                Spinner spinnerPerlengkapan = new Spinner(this);
                List<String> perlengkapan = new ArrayList<String>();
                perlengkapan.add("Mouse");
                perlengkapan.add("Keyboard");
                perlengkapan.add("Printer");
                perlengkapan.add("Lainya");

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, perlengkapan);

                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.weight = 7.0f;

                spinnerPerlengkapan.setLayoutParams(params);
                spinnerPerlengkapan.setAdapter(adapter);
                spinnerPerlengkapan.setId(R.id.spinnerPerlengkapan);
                spinnerPerlengkapan.setOnItemSelectedListener(this);

                information.addView(spinnerPerlengkapan);
            } else if (problem.equals("Email Tidak Berfungsi")){
                TextView txt = new TextView(this);
                txt.setText("Alamat");
                txt.setTextSize(14);
                txt.setTextColor(Color.BLACK);
                LinearLayout.LayoutParams txtParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                txtParams.weight = 3.0f;
                txt.setLayoutParams(txtParams);
                headerInformation.addView(txt);

                EditText et = new EditText(this);
                et.setText("");
                LinearLayout.LayoutParams etParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,7.0f);
                et.setLayoutParams(etParams);
                etGlobal = et;
                information.addView(et);
            } else if (problem.equals("Lainya")){
                TextView txt = new TextView(this);
                txt.setText("Sebutkan");
                txt.setTextSize(14);
                txt.setTextColor(Color.BLACK);
                LinearLayout.LayoutParams txtParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                txtParams.weight = 3.0f;
                txt.setLayoutParams(txtParams);
                txt.setHint("Tuliskan");
                headerInformation.addView(txt);

                EditText et = new EditText(this);
                et.setText("");
                LinearLayout.LayoutParams etParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,7.0f);
                et.setLayoutParams(etParams);
                etGlobal = et;
                information.addView(et);
            }
        } else if(spinner.getId() == R.id.spinnerPerlengkapan){
            Spinner x = findViewById(R.id.spinnerPerlengkapan);
            problem2 = x.getSelectedItem().toString();
            detail = problem2;
            LinearLayout additionalInfo = findViewById(R.id.AdditionalInformation);
            additionalInfo.removeAllViews();
            additionalInfo.setVisibility(View.INVISIBLE);
            EditText txt = new EditText(this);
            txt.setText("");
            txt.setHint("Tuliskan");
            LinearLayout.LayoutParams txtParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT,1.0f);
            txt.setLayoutParams(txtParams);
            if(problem2.equals("Lainya")) {
                additionalInfo.setVisibility(View.VISIBLE);
                additionalInfo.addView(txt);
                etGlobal = txt;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println(resultCode);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            final ImageView imageView = findViewById(R.id.imageView);
            imageView.setVisibility(View.VISIBLE);
            imageView.setImageBitmap(imageBitmap);
        }
        if(requestCode == REQUEST_IMAGE_GALLERY && resultCode == RESULT_OK){
            Uri selectedImage = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(GuestActivity.this.getContentResolver(), selectedImage);
                final ImageView imageView = findViewById(R.id.imageView);
                imageView.setVisibility(View.VISIBLE);
                imageView.setImageBitmap(bitmap);
            } catch (IOException e) {
                Log.i("TAG", "Some exception " + e);
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.logout) {
            SharedPreferences.Editor prefEdit
                    = GuestActivity.this
                    .getSharedPreferences("LOGIN", MODE_PRIVATE).edit();
            prefEdit.clear();
            prefEdit.commit();

            Intent intent = new Intent(GuestActivity.this, LoginActivity.class);
// set the new task and clear flags
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
