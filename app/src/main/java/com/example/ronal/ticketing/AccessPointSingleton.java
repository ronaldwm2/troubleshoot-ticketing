package com.example.ronal.ticketing;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 * Created by Ronal on 5/14/2018.
 */

class AccessPointSingleton {
    private static final AccessPointSingleton ourInstance = new AccessPointSingleton();
    static AccessPointSingleton getInstance() {
        return ourInstance;
    }

    public Boolean loginSuccess = new Boolean(false);
    public String name;
    public String privilege;
    public String search;

    public Boolean isSearch;

    public ArrayList<ProblemClass> problem = new ArrayList<>();
    public ArrayList<String> title = new ArrayList<>();
    public ArrayList<String> desc = new ArrayList<>();

    public RecyclerView recyclerView;
    public FragmentActivity fragmentActivity;
    public Boolean isHistory;

    public int idxRecycler;


    private Context currentContext;

    public List<List<String>> lstOfProblems = new ArrayList();

    public Context getCurrentContext(){
        return currentContext;
    }

    public void setCurrentContext(Context con){
        currentContext = con;
    }

    public static String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                        return inetAddress.getHostAddress();
                    }
                }
            }
        } catch (SocketException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private AccessPointSingleton() {

    }
}
