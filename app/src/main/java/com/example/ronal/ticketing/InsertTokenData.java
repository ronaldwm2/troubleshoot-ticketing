package com.example.ronal.ticketing;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

/**
 * Created by Ronal on 7/4/2018.
 */

public class InsertTokenData extends AsyncTask<String,String,String> {
    ProgressDialog progressDialog;
    @Override
    protected void onPreExecute(){
        super.onPreExecute();
        progressDialog = new ProgressDialog(AccessPointSingleton.getInstance().getCurrentContext());
        progressDialog.setMessage("Retrieving Data");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... arg) {
        String data = "id=" + arg[0] + "&token3rd=" + arg[1];
        WebService ws = new WebService("http://sadess.store/mobile/addThirdPartyToken.php", "POST", data);
        return ws.responseBody;
    }

    @Override
    protected void onPostExecute(String result){
        progressDialog.hide();
        if (result.toLowerCase().contains("success")){
            Toast.makeText(AccessPointSingleton.getInstance().getCurrentContext(),"Sukses",Toast.LENGTH_SHORT).show();
        }
    }


}