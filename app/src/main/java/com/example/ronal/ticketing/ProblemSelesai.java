package com.example.ronal.ticketing;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;

/**
 * Created by Ronal on 7/3/2018.
 */

public class ProblemSelesai extends AsyncTask<String,String,String> {
    @Override
    protected String doInBackground(String... arg) {
        String data = "token=" + arg[0];
        WebService ws = new WebService("http://sadess.store/mobile/problemDone.php", "POST", data);
        return ws.responseBody;
    }
    protected void onPostExecute(String result) {
        AlertDialog.Builder Adialog = new AlertDialog.Builder(AccessPointSingleton.getInstance().getCurrentContext());
        Adialog.setCancelable(true);
        Adialog.setTitle("response");
        Adialog.setMessage(result);
        Adialog.setPositiveButton("Keluar",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        AccessPointSingleton.getInstance().getCurrentContext().startActivity(new Intent(AccessPointSingleton.getInstance().getCurrentContext(),AdminActivity.class));
                    }
                }
        );
        AlertDialog Adialog2 = Adialog.create();
        Adialog2.show();

    }
}
