package com.example.ronal.ticketing;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;

import java.util.List;

/**
 * Created by Ronal on 5/27/2018.
 */

public class InsertToken extends AsyncTask<String,String,String> {
    String token;
    ProgressDialog progressDialog;

    public InsertToken(String problem) {
        this.token = problem;
    }
    @Override
    protected void onPreExecute(){
        super.onPreExecute();
        progressDialog = new ProgressDialog(AccessPointSingleton.getInstance().getCurrentContext());
        progressDialog.setMessage("Retrieving Data");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }
    @Override
    protected String doInBackground(String... arg) {
        String data = "token=" + arg[0] + "&id=" + arg[1] + "&gmbr=" + arg[2] + "&detail=" + arg[3];
        WebService ws = new WebService("http://sadess.store/mobile/insertProblem.php", "POST", data);
        return ws.responseBody;
    }

    @Override
    protected void onPostExecute(String result) {
        progressDialog.hide();
        if (result.equals("success")) {
            AlertDialog.Builder Adialog = new AlertDialog.Builder(AccessPointSingleton.getInstance().getCurrentContext());
            Adialog.setCancelable(true);
            Adialog.setTitle("Nomor token anda");
            Adialog.setMessage(token);
            Adialog.setPositiveButton("Keluar",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    }
            );
            AlertDialog Adialog2 = Adialog.create();
            Adialog2.show();
        } else {
            AlertDialog.Builder Adialog = new AlertDialog.Builder(AccessPointSingleton.getInstance().getCurrentContext());
            Adialog.setCancelable(true);
            Adialog.setTitle("Gagal");
            Adialog.setMessage(result);
            Adialog.setPositiveButton("Keluar",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    }
            );
            AlertDialog Adialog2 = Adialog.create();
            Adialog2.show();
        }
    }
}
