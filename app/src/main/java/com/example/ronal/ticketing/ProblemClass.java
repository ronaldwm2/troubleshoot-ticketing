package com.example.ronal.ticketing;

import java.util.ArrayList;

/**
 * Created by Ronal on 5/27/2018.
 */

public class ProblemClass {
    private String title;
    private String desc;
    private String token;
    private String tanggal;
    private String detil;
    private String token3rd;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    private String nama;

    public String getPermasalahan() {
        return permasalahan;
    }

    public void setPermasalahan(String permasalahan) {
        this.permasalahan = permasalahan;
    }

    private String permasalahan;

    public String getToken3rd() {
        return token3rd;
    }

    public void setToken3rd(String token3rd) {
        this.token3rd = token3rd;
    }

    public ProblemClass(String title, String desc, String token, String tanggal, String nama, String permasalahan, String detil, String token3rd) {
        this.token = token;
        this.tanggal = tanggal;
        this.token3rd = token3rd;

        this.title = title;
        this.desc = desc;
        this.nama = nama;
        this.permasalahan = permasalahan;
        this.detil = detil;
    }

    public String getDetil() {
        return detil;
    }

    public void setDetil(String detil) {
        this.detil = detil;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
