package com.example.ronal.ticketing;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.util.ArrayList;

public class AdminActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        new AdminActivitySeedData().execute();
        AccessPointSingleton.getInstance().isSearch = false;

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AdminActivity.this, GuestActivity.class));
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        displaySelectedScreen(R.id.nav_problem);

        navigationView.setCheckedItem(R.id.nav_problem);


        update();
    }

    void update(){
        new Thread(new Runnable(){
            @Override
            public void run(){
                try{
                    Thread.sleep(2000);
                    if(!AccessPointSingleton.getInstance().isHistory) {
                        new AdminActivitySeedData().execute();
                    }
                    else{
                        new AdminActivitySeedHistoryData().execute();
                    }
                    AdminActivity.this.runOnUiThread(new Runnable(){
                        @Override
                        public void run(){
                            RecyclerViewAdapter adapter = new RecyclerViewAdapter(AccessPointSingleton.getInstance().title, AccessPointSingleton.getInstance().desc, AccessPointSingleton.getInstance().fragmentActivity);

                            AccessPointSingleton.getInstance().recyclerView.setAdapter(adapter);
                            AccessPointSingleton.getInstance().recyclerView.setLayoutManager(new LinearLayoutManager(AccessPointSingleton.getInstance().fragmentActivity));
                        }
                    });
                    update();
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }
        }).start();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(AdminActivity.this);
            alertDialog.setMessage("Are you sure you want to exit?");

            alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
//                    Intent startMain = new Intent(Intent.ACTION_MAIN);
//                    startMain.addCategory(Intent.CATEGORY_HOME);
//                    startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(startMain);
                    moveTaskToBack(true);
                }
            });

            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });

            AlertDialog dialog = alertDialog.create();
            dialog.show();

//            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.admin, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(AdminActivity.this,SortActivity.class));
        } else if (id == R.id.action_search) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(AdminActivity.this);
            alertDialog.setTitle("Search");

            EditText editText = new EditText(AdminActivity.this);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT
            );
            LinearLayout.LayoutParams layoutParam = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT
            );

            editText.setText(AccessPointSingleton.getInstance().search);

            editText.setPaddingRelative(20, 20, 20, 20);
            layoutParams.setMargins(20,10,20,0);

            LinearLayout ll = new LinearLayout(AdminActivity.this);
            ll.addView(editText);

            editText.setLayoutParams(layoutParam);

            ll.setPadding(50,15,50,15);
            ll.setLayoutParams(layoutParams);
            alertDialog.setView(ll);
            final EditText et = editText;
            alertDialog.setPositiveButton("Search", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    AccessPointSingleton.getInstance().isSearch = true;
                    AccessPointSingleton.getInstance().search = et.getText().toString();
                }
            });

            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });

            AlertDialog dialog = alertDialog.create();
            dialog.show();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        displaySelectedScreen(item.getItemId());

        return true;
    }

    private void displaySelectedScreen(int itemId) {

        Fragment fragment = null;

        if (itemId == R.id.nav_problem) {
            // Handle the camera action
            fragment = new FragmentProblem();

        } else if(itemId == R.id.add_Account) {
            fragment = new FragmentAdd();
        }
        else if (itemId == R.id.nav_history) {
            fragment = new FragmentHistory();

        } else if (itemId == R.id.nav_signout) {

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(AdminActivity.this);
            alertDialog.setMessage("Are you sure you want to Sign Out?");

            alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    SharedPreferences.Editor prefEdit
                            = AdminActivity.this
                            .getSharedPreferences("LOGIN", MODE_PRIVATE).edit();
                    prefEdit.clear();
                    prefEdit.commit();
                    Intent intent = new Intent(AdminActivity.this, LoginActivity.class);
// set the new task and clear flags
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            });

            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });

            AlertDialog dialog = alertDialog.create();
            dialog.show();

        }

        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }
}
