package com.example.ronal.ticketing;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class DetailProblem extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_problem);
        setTitle("Detail Problem");

        TextView tvSelesai = findViewById(R.id.tvSelesai);
        if(AccessPointSingleton.getInstance().isHistory){
            tvSelesai.setText("Selesai");
        }else tvSelesai.setText("Belum Selesai");

        AccessPointSingleton.getInstance().setCurrentContext(DetailProblem.this);

        TextView detailNama = findViewById(R.id.detailNama);
        TextView detailTanggal = findViewById(R.id.detailJam);
        final TextView detailToken = findViewById(R.id.detailToken);
        TextView masalah = findViewById(R.id.textViewMasalah);
        TextView pihak3 = findViewById(R.id.tokenLuar);
        TextView detailDetail = findViewById(R.id.DetailTextView);

        pihak3.setText(AccessPointSingleton.getInstance().problem.get(AccessPointSingleton.getInstance().idxRecycler).getToken3rd());

        detailNama.setText(AccessPointSingleton.getInstance().problem.get(AccessPointSingleton.getInstance().idxRecycler).getNama());
        detailTanggal.setText(AccessPointSingleton.getInstance().problem.get(AccessPointSingleton.getInstance().idxRecycler).getTanggal());
        detailToken.setText(AccessPointSingleton.getInstance().problem.get(AccessPointSingleton.getInstance().idxRecycler).getToken());
        masalah.setText(AccessPointSingleton.getInstance().problem.get(AccessPointSingleton.getInstance().idxRecycler).getPermasalahan());
        detailDetail.setText(AccessPointSingleton.getInstance().problem.get(AccessPointSingleton.getInstance().idxRecycler).getDetil());


        System.out.println(AccessPointSingleton.getInstance().idxRecycler);

        Button btnSelesai = findViewById(R.id.selesaiBtn);
        btnSelesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(DetailProblem.this);
                alertDialog.setCancelable(true);
                alertDialog.setTitle("Konfirmasi");
                alertDialog.setMessage("Problem Sudah Selesai?");
                alertDialog.setPositiveButton("Sudah",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                new ProblemSelesai().execute(detailToken.getText().toString());
                            }
                        });
                alertDialog.setNegativeButton("Belum",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        });
                AlertDialog dialog = alertDialog.create();
                dialog.show();
            }
        });
        Button btn = findViewById(R.id.hubungiPihak3Btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(DetailProblem.this);
                alertDialog.setCancelable(true);
                alertDialog.setTitle("Hubungi Pihak Ke Tiga");

                LinearLayout linearLayout = new LinearLayout(DetailProblem.this);
                linearLayout.setOrientation(LinearLayout.VERTICAL);

                EditText inp = new EditText(DetailProblem.this);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);

                inp.setLayoutParams(lp);

                TextView text = new TextView(DetailProblem.this);
                text.setText("Nomor Token");
                text.setLayoutParams(lp);
                linearLayout.addView(text);

                EditText inp2 = new EditText(DetailProblem.this);
                inp2.setLayoutParams(lp);
                linearLayout.addView(inp2);

                alertDialog.setView(linearLayout);
                final EditText last = inp2;
                alertDialog.setPositiveButton("Benar",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                new InsertTokenData().execute(detailToken.getText().toString(),last.getText().toString());
                            }
                        });
                alertDialog.setNegativeButton("Salah", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });

                AlertDialog dialog = alertDialog.create();
                dialog.show();
            }
        });
    }
}
