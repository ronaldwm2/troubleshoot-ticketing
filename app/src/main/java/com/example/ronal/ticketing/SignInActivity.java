package com.example.ronal.ticketing;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Ronal on 5/14/2018.
 */

public class SignInActivity extends AsyncTask<String,String,String> {
    private TextView username,password;
    private Context target;
    private Boolean isGet;
    ProgressDialog progressDialog;
    public SignInActivity(){
        super();
    }

    public SignInActivity(Context target,boolean isGet){
        this.target = target;
        this.username = username;
        this.password = password;
        this.isGet = isGet;
    }

    @Override
    protected void onPreExecute(){
        super.onPreExecute();
        progressDialog = new ProgressDialog(AccessPointSingleton.getInstance().getCurrentContext());
        progressDialog.setMessage("Retrieving Data");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... arg0){
        if(!isGet){
            try{
                String username = (String) arg0[0];
                String password = (String) arg0[1];
                String data = "username=" + arg0[0]+"&password="+arg0[1];
                WebService ws = new WebService("http://sadess.store/mobile/user.php","POST",data);
                if(ws.responseBody != ""){
                    AccessPointSingleton.getInstance().loginSuccess = true;
                    return ws.responseBody;
                }
            }catch(Exception ex){
                return new String("Exception error : " + ex.getMessage());
            }
        }
        return null;
    }


    @Override
    protected void onPostExecute(String result){
        progressDialog.hide();
        if(AccessPointSingleton.getInstance().loginSuccess){
            String[] data = result.split("-PRIVILEGE-");
            AccessPointSingleton.getInstance().privilege = data[1];
            AccessPointSingleton.getInstance().name = data[0];

            Toast.makeText(target, "Welcome " + AccessPointSingleton.getInstance().name + "\nPRIVILEGE " + AccessPointSingleton.getInstance().privilege, Toast.LENGTH_SHORT).show();

            SharedPreferences.Editor prefEdit = target.getSharedPreferences("LOGIN", MODE_PRIVATE).edit();
            prefEdit.putString("LEVEL", AccessPointSingleton.getInstance().privilege);
            prefEdit.putString("ID", AccessPointSingleton.getInstance().name);
            prefEdit.commit();

            SharedPreferences getPref = target.getSharedPreferences("LOGIN",MODE_PRIVATE);

            if(getPref.getString("LEVEL","").equals("admin")) target.startActivity(new Intent(target, AdminActivity.class));
            else if(getPref.getString("LEVEL","").equals(""))Toast.makeText(AccessPointSingleton.getInstance().getCurrentContext(),"ID/PASSWORD salah atau belum terkoneksi", Toast.LENGTH_SHORT).show();
            else target.startActivity(new Intent(target,GuestActivity.class));
        }
        else {
            Toast.makeText(target, "Id or Password is wrong", Toast.LENGTH_SHORT).show();
        }
    }

}
