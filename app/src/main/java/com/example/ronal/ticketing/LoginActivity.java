package com.example.ronal.ticketing;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

public class LoginActivity extends AppCompatActivity {

    EditText id_edit;
    EditText pass_edit;

    public static String nama,password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_login);

        AccessPointSingleton.getInstance().setCurrentContext(LoginActivity.this);

        final LinearLayout layout = findViewById(R.id.loginLinearLayout);
        Glide.with(this).load(R.drawable.login_image).asBitmap().into(new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                Drawable drawable = new BitmapDrawable(getApplicationContext().getResources(),resource);
                layout.setBackground(drawable);
            }
        });


        id_edit = findViewById(R.id.editText);
        pass_edit = findViewById(R.id.editText2);

        Button guestBtnHandler = findViewById(R.id.guest_login_btn);
        guestBtnHandler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(LoginActivity.this,GuestActivity.class);
                startActivity(i);
            }
        });

        final Button adminBtnHandler = findViewById(R.id.admin_login_btn);
        adminBtnHandler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SignInActivity(LoginActivity.this,false).execute(id_edit.getText().toString(),pass_edit.getText().toString());

                SharedPreferences pref = getSharedPreferences("LOGIN",MODE_PRIVATE);
//                if (id_edit.getText().toString().equals("admin")
//                        && pass_edit.getText().toString().equals("admin")) {
//
//
//                    SharedPreferences.Editor prefEdit
//                            = getSharedPreferences("LOGIN", MODE_PRIVATE).edit();
//                    prefEdit.putString("LEVEL", "admin");
//                    prefEdit.commit();
//
//                    startActivity(new Intent(LoginActivity.this, AdminActivity.class));
//                }

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        SharedPreferences preferences
                = getSharedPreferences("LOGIN", MODE_PRIVATE);

        if (preferences.getString("LEVEL", "").equals("admin")) {
            startActivity(new Intent(LoginActivity.this, AdminActivity.class));
        }else if ((!preferences.getString("LEVEL","").equals("")) && (!preferences.getString("LEVEL","").equals("admin"))){
            startActivity(new Intent(LoginActivity.this,GuestActivity.class));
        }
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);

        super.onBackPressed();
    }
}
