package com.example.ronal.ticketing;

import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Ronal on 5/25/2018.
 */

public class Token  extends AsyncTask<String,String,List<List<String>>> {
    String problem;
    String nama;
    String blob;
    String detail;

    public Token(String problem,String nama,String blob,String detail) {
        this.problem = problem;
        this.nama = nama;
        this.blob = blob;
        this.detail = detail;
    }
    public Token(){

    }

    @Override
    protected List<List<String>> doInBackground(String... strings) {
        WebService ws = new WebService("http://sadess.store/mobile/fetchAllProblem.php","POST","");
        List<String> lstOfResponse = new ArrayList<String>();
        lstOfResponse.addAll(Arrays.asList(ws.responseBody.split("<!#@SEPARATOR@#!>")));
        //AccessPointSingleton.getInstance().title;
        List<String> lstOfP1 = new ArrayList<>();
        List<String> lstOfP2 = new ArrayList<>();
        List<String> lstOfP3 = new ArrayList<>();
        List<String> lstOfP4 = new ArrayList<>();
        for(String str : lstOfResponse){
            if(str.contains("P1")) lstOfP1.add(str);
            else if(str.contains("P2")) lstOfP2.add(str);
            else if(str.contains("P3")) lstOfP3.add(str);
            else if(str.contains("P4")) lstOfP4.add(str);
            else System.out.println(str + "is illegal\n");
        }
        List<List<String>> lstOfAllResponses = new ArrayList<>();
        lstOfAllResponses.add(lstOfP1);
        lstOfAllResponses.add(lstOfP2);
        lstOfAllResponses.add(lstOfP3);
        lstOfAllResponses.add(lstOfP4);

        AccessPointSingleton.getInstance().lstOfProblems = new ArrayList<>();
        AccessPointSingleton.getInstance().lstOfProblems = lstOfAllResponses;
        return null;
    }

    @Override
    protected void onPostExecute(List<List<String>> ans){
        int indexParam = problem.equals("Perlengkapan Rusak") ? 0 : problem.equals("Internet Mati") ? 1 : problem.equals("Email Tidak Berfungsi") ? 2 : 3;
        List<String> tokenTarget = AccessPointSingleton.getInstance().lstOfProblems.get(indexParam);
        int highest = 0;
        for(String row : tokenTarget){
            if(Integer.parseInt(row.substring(3)) > highest) highest = Integer.parseInt(row.substring(3));
        }
        System.out.println(highest);
        final String token = indexParam == 0 ? "P1T" + String.valueOf(highest+1) : indexParam == 1 ? "P2T" + String.valueOf(highest+1) : indexParam == 2 ? "P3T" + String.valueOf(highest+1) : "P4T" + String.valueOf(highest+1);
        new InsertToken(token).execute(token,nama,blob,detail);
    }


}
