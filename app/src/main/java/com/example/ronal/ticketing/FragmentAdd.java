package com.example.ronal.ticketing;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by Ronal on 7/4/2018.
 */

public class FragmentAdd extends Fragment {
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        return super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.fragment_add_account, container, false);

        Button btn = view.findViewById(R.id.add_account_btn);

        final EditText name = view.findViewById(R.id.etName);
        final EditText pw = view.findViewById(R.id.etPw);
        final EditText jb = view.findViewById(R.id.etJb);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new InsertNewAccount().execute(name.getText().toString(),pw.getText().toString(),jb.getText().toString());
            }
        });
        return view;
    }
}
