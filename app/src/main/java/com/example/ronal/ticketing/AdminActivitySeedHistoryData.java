package com.example.ronal.ticketing;

import android.os.AsyncTask;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by Ronal on 5/27/2018.
 */

public class AdminActivitySeedHistoryData extends AsyncTask<Void,Void,String> {
    @Override
    protected String doInBackground(Void... voids) {
        WebService ws = new WebService("http://sadess.store/mobile/getProblemForSeedHistoryData.php","POST","");

        return ws.responseBody;
    }

    protected void onPostExecute(String string){

        List<String> dataRow = new ArrayList<>(Arrays.asList(string.split("<data>")));
        AccessPointSingleton.getInstance().problem = new ArrayList<>();
        AccessPointSingleton.getInstance().title = new ArrayList<>();
        AccessPointSingleton.getInstance().desc = new ArrayList<>();
        for(String temp : dataRow){
            temp.replaceAll("</data>","");
            if(!temp.contains("<idProblem>"))continue;

            String idProblem = temp.substring(temp.indexOf("<idProblem>")+("<idProblem>").length(),temp.indexOf("</idProblem>"));
            String detail = temp.substring(temp.indexOf("<detil>")+("<detil>").length(),temp.indexOf("</detil>"));
            String idAkun = temp.substring(temp.indexOf("<idAkun>")+("<idAkun>").length(),temp.indexOf("</idAkun>"));
            String timeTemp = temp.substring(temp.indexOf("<entered>")+("<entered>").length(),temp.indexOf("</entered>"));
            String idToken = temp.substring(temp.indexOf("<idToken>")+("<idToken>").length(),temp.indexOf("</idToken>"));
            String id3rd = temp.substring(temp.indexOf("<idThird>")+("<idThird>").length(),temp.indexOf("</idThird>"));
            try{
                SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                Date parsedDate = date.parse(timeTemp);
                Timestamp timeStamp = new Timestamp(parsedDate.getTime());
                System.out.println(idProblem + " " + idAkun + " " + timeStamp.toString());
                String permasalahan = idProblem.equals("P1") ? "Perlengkapan Rusak" : idProblem.equals("P2") ? "Internet Mati" : idProblem.equals("P3") ? "Email Tidak Berfungsi" : "Lainya";
                if(!AccessPointSingleton.getInstance().isSearch){
                    AccessPointSingleton.getInstance().problem.add(new ProblemClass(idAkun, permasalahan + " " + timeStamp.toString(), idToken, timeStamp.toString(), idAkun, permasalahan, detail,id3rd));
                    AccessPointSingleton.getInstance().title.add(idAkun + " | " + permasalahan);
                    AccessPointSingleton.getInstance().desc.add(timeStamp.toString());
                }else{
                    if(idAkun.toLowerCase().contains(AccessPointSingleton.getInstance().search.toLowerCase()) || permasalahan.toLowerCase().contains(AccessPointSingleton.getInstance().search.toLowerCase()) || idToken.toLowerCase().contains(AccessPointSingleton.getInstance().search.toLowerCase())){
                        AccessPointSingleton.getInstance().problem.add(new ProblemClass(idAkun, permasalahan + " " + timeStamp.toString(), idToken, timeStamp.toString(), idAkun, permasalahan, detail,id3rd));
                        AccessPointSingleton.getInstance().title.add(idAkun + " | " + permasalahan);
                        AccessPointSingleton.getInstance().desc.add(timeStamp.toString());
                    }
                }

            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
